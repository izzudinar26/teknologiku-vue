import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/home.vue'
import Article from '../views/article.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/article/:articleId',
    name: 'Artikel',
    component: Article,
    props: true
  }
]

const router = new VueRouter({
  mode: 'history',
  routes
})

export default router
