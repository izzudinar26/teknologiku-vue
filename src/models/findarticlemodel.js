import Vue from 'vue'
import axios from 'axios'

export default new Vue({
  data: () => ({
    title: '',
    body: ''
  }),
  methods: {
    async getData (id) {
      axios.post('https://unruffled-golick-0043c5.netlify.com/.netlify/functions/api/article', { postId: id })
        .then(response => {
          this.title = response.data.title
          this.body = response.data.content
          document.title = response.data.title
        })
        .catch(err => console.log(err))
    }
  }
})
