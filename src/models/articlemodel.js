import Vue from 'vue'
import axios from 'axios'

export default new Vue({
  data: () => ({
    article: []
  }),
  methods: {
    async fetchdata () {
      axios.get('https://unruffled-golick-0043c5.netlify.com/.netlify/functions/api')
        .then(res => {
          this.article = res.data
        })
        .catch(err => console.log(err))
    }
  }
})
